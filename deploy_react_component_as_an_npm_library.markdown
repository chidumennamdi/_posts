# Deploy React component as an NPM library

# Introduction

Since the advent of Node.js in 2009, everything we knew about JavaScript changed. The seemingly dying language made phoenix-like comeback, growing to become the most popular language in the world.

JavaScript was earlier seen as a web-browser's language, but Node.js came and made it server-side. In essence, Node.js allows developers to develop web-servers with JavaScript. Thus, JavaScript was not only used in browsers, but also in development of web servers.

In January 2010, NPM was introduced for the Node.js environment. It makes it easier for developers to publish and share source code of JavaScript libraries, developers can then use the code by installing the library and `import`ing it in his code.

NPM has since been the de-facto software registry for JavaScript and Node.js libaries. Many frameworks has emerged using NPM to distribute their library. `React`, `Vue`, `Angular`, etc. apps are developed using NPM. You either install their boilerplates or install their official CLI tool. All through NPM, and of course Node.js must be installed.

Right now, there are billions of libraries in NPM. Angular,React and its cousins are all imported from NPM, then modules dependent on these frameworks are also hosted in NPM. Normally, it is quite easier to write and host a JS library in NPM because it not dependent on any framework, just pull it in and we are good to go. The challenge here is how do we write and publish a module dependent on a JS framework to be used as an NPM library.

That's what we are going to solve here and we will be developing a library for the React.js framework.

In this tutorial, we are going to see how to create a `React` component and also demonstrate how to deploy it to NPM to be consumed as a library.

As a demo, we are going to build a countdown timer React component. 

A countdown timer is used to display the count down to any event. Like, in wedding anniversary, countdown timers can be used to cut the cake. You know the popular: "10! 9! 8! ...0!"

So, we are going develop our own countdown timer for the React framework, so that it can be used by other devs we they need a timer in their React apps. They just pull in our library, instead of re-inventing the wheel.

The source code we are going to build in this article can be found [here.](https://github.com/philipszdavido/countdown-timer)

We will also test our library using `Jest` framework.

# Project goals

Here are list of things we are going to achieve in this article:

* 

# Assumptions

I'll assume you are proficient with these tools and frameworks:

* Node.js, NPM
* React.js
* Git
* JavaScript, ES6, and CSS

Also, make sure you have `Node.js`, IDE (`Visual Studio Code`, `Atom`), and `Git` all installed.
`NPM` comes with `Node.js`, it does need separate installation.

# Project setup

Let's setup our project directory. I'll call mine `countdown-timer`, Inside that, we will create `src` directory for sources and `test` directory for unit tests:

```sh
mkdir countdown-timer
cd countdown-timer
mkdir src && mkdir test
```
After that, the directory `countdown-timer`, will look like this:

```
+- countdown-timer
 +- src
 +- test
```

Next, we are going to make our directory a Node.js project directory:

```sh
npm init -y
```

This command creates a `package.json` file with our basic information we supplied to NPM. package.json is the most important file in a Node.js project, it is used to let NPM know some basic things about our project and, crucially, the external NPM packages it depends on.

# Install Dependencies

We install libraries that are important in our development process:

```
npm i react -D
```
We installed the `react` library as an dev Dependency, this is so because we don't want NPM to download it again when the user installs our library because the user would have alreday installed the `react` library in his React app.

So after the above command, our `package.json`, will look like this:

```json
{
  "name": "countdown-timer",
  "version": "1.0.0",
  "description": "A React library used to countdown time",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://github.com/philipszdavido/countdown-timer.git"
  },
  "keywords": [],
  "author": "Chidume Nnamdi <kurtwanger40@gmail.com>",
  "license": "ISC",
  "bugs": {
    "url": "https://github.com/philipszdavido/countdown-timer/issues"
  },
  "homepage": "https://github.com/philipszdavido/countdown-timer#readme",
  "devDependencies": {
    "react": "^16.3.2"
  }
}
```

Next, we create `countdown.js` in the `src` folder:

```sh
touch src/countdown.js
```
`countdown.js` will contain our code implementation. We wont go down to explain our code. You can just add anything, maybe a text, `"Holla! My First Component"`. It doesn't matter, all you have to know is the essential configurations needed to deploy and use a React component as a library.

To create a React component, we must first import `React` and `Component` from the `react` library.
```js
// src/countdown.js
import React, { Component } from 'react'
```

# Deploy to NPM



# Conclusion


